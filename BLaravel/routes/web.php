<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard_Controller;
use App\Http\Controllers\Daftar_Controller;
use App\Http\Controllers\Cast_Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Dashboard_Controller::class, 'utama']);
Route::get('/daftar', [Daftar_Controller::class, 'regis']);
Route::post('/home', [Daftar_Controller::class, 'home']);
Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});

// CRUD Cast

// C = Create Data
Route::get('/cast/create', [Cast_Controller::class, 'create']);
Route::post('/cast', [Cast_Controller::class, 'store']);

// R = Read Data
Route::get('/cast', [Cast_Controller::class, 'index']);
Route::get('/cast/{id}', [Cast_Controller::class, 'show']);

// U = Update Data
Route::get('/cast/{id}/edit', [Cast_Controller::class, 'edit']);
Route::put('/cast/{id}', [Cast_Controller::class, 'update']);

// D = Delete Data
Route::delete('/cast/{id}', [Cast_Controller::class, 'destroy']);