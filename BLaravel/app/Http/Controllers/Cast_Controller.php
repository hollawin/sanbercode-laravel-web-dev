<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class Cast_Controller extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        //validasi
        $validated = $request->validate([
            'nama' => 'required|max:255|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //insert data ke database
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio'  => $request->input('bio')
        ]);

        //return redirect 
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        //validasi
        $validated = $request->validate([
            'nama' => 'required|max:255|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //update  data ke database
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]);

        //return redirect 
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
