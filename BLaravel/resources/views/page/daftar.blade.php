@extends('layouts.master')
@section('title')
    Halaman Pendaftaran  
@endsection
@section('sub-title')
    Pendaftaran
@endsection
@section('content')
    <form action="/home" method="POST">
        @csrf
        <label>Firs Name</label> <br>
        <input type="text" name="fname"> <br><br>

        <label>Last Name</label> <br>
        <input type="text" name="lname"> <br><br>

        <label>Gender</label> <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br><br>

        <label>Nationality</label> <br>
        <select name="" id="">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
            <option value="singapura">Singapura</option>
        </select><br><br>

        <label>Languange Spoken</label> <br>
        <input type="checkbox" name="skill">Bahasa Indonesia <br>
        <input type="checkbox" name="skill">English <br>
        <input type="checkbox" name="skill">Other <br><br>
        
        <label>Bio</label><br>
        <textarea name="message"cols="50" rows="10"></textarea> <br><br>

    <input type="submit" value="kirim">

    </form>
    
@endsection