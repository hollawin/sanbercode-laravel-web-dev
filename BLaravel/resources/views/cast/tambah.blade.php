@extends('layouts.master')
@section('title')
    Halaman Tambah Cast  
@endsection
@section('sub-title')
    Cast
@endsection
@section('content')
  <form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama">
    </div>

    @error('nama')
    <div class="alert alert-danger" role="alert">
        {{$message}}
      </div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control  @error('nama') is-invalid @enderror" name="umur">
    </div>

    @error('umur')
    <div class="alert alert-danger" role="alert">
        {{$message}}
      </div>
    @enderror

    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control  @error('nama') is-invalid @enderror" id="" cols="30" rows="10"></textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger" role="alert">
        {{$message}}
      </div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection