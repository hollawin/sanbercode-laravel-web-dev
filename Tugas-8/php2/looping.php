<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php 
    echo "<h3> Soal No 1 Looping I Love PHP </h3>";

    //looping pertama
    echo "<h5>Looping Pertama</h5>";
    for ($i=2; $i<=20; $i+=2){
        echo "$i - I Love PHP  <br>";
    }

    //looping kedua
    echo "<h5>Looping Kedua</h5>";
    for ($a=20; $a>=2; $a-=2){
        echo "$a - I Love PHP  <br>";
    }

    echo "<h3> Soal No 2 Looping Array Modulo </h3>";
    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array nubers : ";
    print_r($numbers);
    echo "<br>";
    echo "Array sisa baginya adalah : ";
    foreach($numbers as $value){
        $rest[] = $value %= 5;
    }
    print_r($rest);
    echo "<br>";

    echo "<h3> Soal No 3 Looping Asociativate Array </h3>";
    $items = [
        ["001" , "Keyboard Logitek" , "60000" , "Keyboard yang mantap untuk kantoran" , "logitek.jpeg"],
        ["002" , "Keyboard MSI", "30000" , "Keyboard gaming MSI mekanik" , "msi.jpeg"],
        ["003" , "Mouse Genius" , "50000" , "Mouse Genius biar lebih pintas" , "genius.jpeg"],
        ["004" , "Mouse Jerry" , "30000" , "Mouse yang disukai kucing" , "jerry.jpeg"],
    ];
    foreach($items as $key => $value){
        $biodata = array(
            "id" => $value[0],
            "Name" => $value[1],
            "Price" => $value[2],
            "Description" => $value[3],
            "Source" => $value[4]
        );
        print_r($biodata);
        echo "<br>";
    };

    echo "<h3> Soal No 4 Asterix </h3>";
    for($a=1; $a<=5; $a++){
        for($b=1; $b<=$a; $b++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>