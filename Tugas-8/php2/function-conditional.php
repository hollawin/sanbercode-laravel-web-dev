<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=h1, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php 
    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($name){
        echo "Hello " . $name . ", Selamat Datang di Sanbercode!";
        echo "<br>";
    }
    greetings("Bagas");
    greetings("Wahyu");
    greetings("Widy");
    echo "<br>";


    echo "<h3> Soal No 2 Reverse String </h3>";

    function reverse($kata1){
        $panjangKata  = strlen($kata1);
        $tampung = "";
        for($i=($panjangKata - 1); $i>=0; $i--){
            $tampung .= $kata1[$i];
        }
        return $tampung;
    }
    function reverseString($kata2){
        $string=reverse($kata2);
        echo $string . "<br>";
    }
    reverseString("abduh");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";


    echo "<h3>Soal No 3 Palindrome </h3>";
    function palindrome($kata3){
        $balikkata = reverse($kata3);
        if ($kata3 == $balikkata){
            echo $kata3 . " = True <br>";
        }else{
            echo $kata3 . " => False <br>";
        }
    }
    palindrome("Civic"); // true
    palindrome("Nababan"); // true
    palindrome("Jambaban"); // false
    palindrome("Racecar"); // true
    echo "<br>";
    

    echo "<h3> Soal No 4 Tentukan Nilai </h3> ";
    function tentukan_nilai($nilai){
        if($nilai >=85 && $nilai <=100){
            return $nilai . " Sangat Baik <br>";
        }else if($nilai >=70 && $nilai <=85 ){
            return $nilai . " Baik <br>";
        }else if($nilai >=60 && $nilai <=70 ){
            return $nilai . " Cukup <br>";
        }else{
            return $nilai . " Kurang <br>";
        }
    }
    echo tentukan_nilai (98);
    echo tentukan_nilai (76);
    echo tentukan_nilai (67);
    echo tentukan_nilai (43);
    ?>
</body>
</html>