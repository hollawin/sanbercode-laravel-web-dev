<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php 
    echo "<h3>Soal No 1</h3>";
    //first sentance
    $kalimat1 = "Hello PHP!";

    echo "Kalimat 1 : " . $kalimat1 . "<br>";
    echo "Panjang Kalimat 1 : " . strlen($kalimat1) . "<br>";
    echo "Jumlah Kalimat 1 : " . str_word_count($kalimat1) . "<br><br>";

    //seconf sentance
    $kalimat2 = "I'm ready for the challenges";
    echo "Kalimat 2 : " . $kalimat2 . "<br>";
    echo "Panjang Kalimat 2 : " . strlen($kalimat2) . "<br>";
    echo "Jumlah Kalimat 2 : " . str_word_count($kalimat2) . "<br>";


    echo "<h3>Soal No 2</h3>";
    $kalimat3 = "I love PHP";

    echo "kata 1 kalimat 3 : " . substr($kalimat3, 0, 1) . "<br>";
    echo "kata 2 kalimat 3 : " . substr($kalimat3, 2, 4) . "<br>";
    echo "kata 3 kalimat 3 : " . substr($kalimat3, 7, 3) . "<br>";


    echo "<h3>Soal No 3</h3>";
    $kalimat4 = "PHP is old but sexy!";
    
    echo "Sebelum di ganti : " . $kalimat4 . "<br>";
    echo "Setelah di ganti : " . str_replace("sexy" , "awesome" , $kalimat4);

    ?>
</body>
</html>